﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Blunt;
using Blunt.Sound;
using Blunt.Synthesis;
using Blunt.Sound.GenerativeMusic;
using Blunt.Probability.Music;
public class AlgorithmicSound : MonoBehaviour
{

	public Mixer outputMixer;
	private Mixer.MixerChannel mixerChannel;
	public string mixerChannelToUse;
	private FMSynth synth;

	private Sequencer sequencer;
	private Utils.CheapRandom randomizer = new Utils.CheapRandom();
	private CallbackSequencer cbseq;
	private bool isInWater = false;
	private bool isInGame = false;
	private bool isQuiet = false;
	private CallbackSequencer.Token utilityToken, pickupToken, quietToken, puzzleSolvedToken;

	private SoundPrefabs.Instruments.Strings endingChordInstrument;
	private SoundPrefabs.WaterSplash pickupSplash;
	private SoundPrefabs.Instruments.RewardPiece rewardInstrument;

	MusicPiece menuMusic, mainMusic, waterMusic;
	Sequencer.NoteInfo[] endingChord;

	public enum Stage
	{
		Start,
		Quiet,
		Puzzle,
		All
	};

	public Stage stage;

	private double[] rewardRhythm =
	{
		0, 0.75, 0.50, 0.25, 0.5 
	};

	private int[] rewardMelody =
	{
		0, 2, 0, 2, 4
	};

	private int rewardPosition;

	public enum Key
	{
		DMinor,
		FMajor
	}

	int[] DMinorFromA4 =
	{
		5 + 0,
		5 + 2,
		5 + 3,
		5 + 5,
		5 + 7,
		5 + 8,
		5 + 10
	};

	public static Key currentKey = Key.DMinor;
	#region TimeSequences
		TimeSequence[] mainKickPatterns = new TimeSequence[]
		{
			// Main patterns quantized on bars.
			new TimeSequence(new double[] {0}, 
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.25f),
			new TimeSequence(new double[] {0, 2.5 }, 
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.25f),
			new TimeSequence(new double[] {0, 2},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.15f),
			new TimeSequence(new double[] {0, 2, 0.5},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.05f),

			// variations, small patterns
			new TimeSequence(new double[] {1},
				PlayHead.createTimeOffset(0, 0, 0, 0.5f), 0.05f),
			new TimeSequence(new double[] {0.5, 1.5}, 
				PlayHead.createTimeOffset(0, 1, 0, 0), 0.05f)
		};

		TimeSequence[] mainSnarePatterns = new TimeSequence[]
		{
			// main patterns
			new TimeSequence(new double[] {1, 1.25, 0.75},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.80f),
			new TimeSequence(new double[] {3},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.10f),
			new TimeSequence(new double[] {1},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.10f)
		};

		TimeSequence[] mainLFOPatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {0}, 
				PlayHead.createTimeOffset(1, 0, 0, 0), 1)
		};


		TimeSequence[] mainBassPatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {0, 2.5, 1, 0.5, 1, 1.5},
				PlayHead.createTimeOffset(2, 0, 0, 0), 0.9f),
			new TimeSequence(new double[] {0, 1, 1, 1, 1, 1, 2},
				PlayHead.createTimeOffset(2, 0, 0, 0), 0.1f)
		};

		TimeSequence[] mainHiHatPatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {0, 0.5, 0.5, 0.5, 1, 1},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.8f),
			new TimeSequence(new double[] {0, 0.5, 0.5, 0.5, 1, 0.5, 0.25, 0.25},
				PlayHead.createTimeOffset(1, 0, 0, 0), 0.2f)
		};

		TimeSequence[] slowHiHatPatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {1, 2, 1, 1, 1},
				PlayHead.createTimeOffset(1, 0, 0, 0), 1.0f)
		};

		TimeSequence[] mainStringPatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {0, 4, 4, 4},
				PlayHead.createTimeOffset(1, 0, 0, 0), 1)
		};

		TimeSequence[] metalloPhonePatterns = new TimeSequence[]
		{
			new TimeSequence(new double[] {0.25, 1}, 0, 0.25f),
            new TimeSequence(new double[] {0.75, 2}, 0, 0.25f),
			new TimeSequence(new double[] {0.125, 0.5}, 0, 0.25f),
			new TimeSequence(new double[] {0.625, 3}, 0, 0.25f)
		};
	#endregion


	void initializeMainMusic(MusicPiece m)
	{
		RandomSequenceList rsl;
		
		var kicks = new SoundPrefabs.Kick(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainKickPatterns);
		kicks.initializeWithVoices(2);
		m.createAndAddInstrument(kicks, rsl,
			p =>
			{
				if(isQuiet || isInWater)
					return null;
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[1];
				notes[0].length = 2;
				notes[0].pitch = EnvironmentTuning.pitchFromNote(EnvironmentTuning.A4 + randomizer.randomRange(-4, 4));
				notes[0].velocity = -15;
				notes[0].enabled = true;
				return notes;
			}
		);

		var strings = new SoundPrefabs.Instruments.Strings(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainStringPatterns);
		strings.initializeWithVoices(9);
		m.createAndAddInstrument(strings, rsl,
			p =>
			{
				if(isInWater || stage == Stage.Start)
					return null;
                Sequencer.NoteInfo[] notes;
                int reduced = p.bars & 0x3;
				if(reduced == 0)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;
				}
				else if (reduced == 1)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				else if(reduced == 2)
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;


					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;
				}
				else
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				return notes;
			}
		);
		
		var hihat = new SoundPrefabs.Instruments.HiHat(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainHiHatPatterns);
		hihat.initializeWithVoices(2);
		m.createAndAddInstrument(hihat, rsl,
			p =>
			{
				if(isQuiet || isInWater)
					return null;
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[1];
				notes[0].length = 0.5f;
				notes[0].velocity = -(21 + randomizer.random01() * 3);
				notes[0].enabled = true;
				return notes;
			}
		);

		var bass = new SoundPrefabs.Instruments.FMBass(synth);
		rsl = new RandomSequenceList();
		rsl.addTimeSequences(mainBassPatterns);
		bass.initializeWithVoices(3);
		m.createAndAddInstrument(bass, rsl,
			p =>
			{
				if(isQuiet || isInWater || (stage == Stage.Puzzle))
					return null;
				float pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 3);
				float length = 0.5f;
				if(currentKey == Key.FMajor)
				{
					pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3);
				}
                else if(currentKey == Key.DMinor)
				{
					// are we at third or more bar?
					if((p.bars & 0x3) > 1 )
					{
						pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3);
						// in first bar?
						if((p.bars & 0x1) == 0)
						{
							if(p.beats == 3)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3);
								length = 0.5f;
							}
							else if(p.beats > 1)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 3);
								length = 1.5f;
							}

						}
						else
						{
							pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 3);
							if(p.beats >= 2)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3);
								length = 1.5f;
							}
						}
					}
					else
					{
						// in first bar?
						if((p.bars & 0x1) == 0)
						{
							if(p.beats == 3)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 3);
								length = 0.5f;
							}
							else if(p.beats > 1)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.G, 3);
								length = 1.5f;
							}

						}
						else
						{
							if(p.beats >= 2)
							{
								pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 3);
								length = 1.5f;
							}
						}
					}

				}
				else
				{

				}
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[1];
				notes[0].length = length;
				notes[0].pitch = pitch;
				notes[0].velocity = -(20 + randomizer.random01() * 3);
				notes[0].enabled = true;
				return notes;
			}
		);

		
		var lfochords = new SoundPrefabs.Instruments.LFOChords(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainLFOPatterns);
		lfochords.initializeWithVoices(6);
		m.createAndAddInstrument(lfochords, rsl,
			p =>
			{
				if(isInWater || stage == Stage.Start)
					return null;
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[3];
				notes[0].length = 3.5f;
				notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
				notes[0].velocity = -24;
				notes[0].enabled = true;

				notes[1].length = 3.5f;
				notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
				notes[1].velocity = -24;
				notes[1].enabled = true;

				notes[2].length = 3.5f;
				notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
				notes[2].velocity = -24;
				notes[2].enabled = true;

				return notes;
			}
		);

		var snare = new SoundPrefabs.Snare(synth);
		rsl = new RandomSequenceList();
		rsl.addTimeSequences(mainSnarePatterns);
		snare.initializeWithVoices(2);
		m.createAndAddInstrument(snare, rsl,
			p =>
			{
				if(isQuiet)
					return null;
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[1];
				notes[0].length = 1f + randomizer.random01() * 0.5f;
				notes[0].enabled = true;
				notes[0].velocity = -6;
				return notes;
			}
		);

	}

	void initializeWaterMusic(MusicPiece m)
	{
		RandomSequenceList rsl;
		var strings = new SoundPrefabs.Instruments.Strings(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainStringPatterns);
		strings.initializeWithVoices(9);
		m.createAndAddInstrument(strings, rsl,
			p =>
			{
				Sequencer.NoteInfo[] notes;
				int reduced = p.bars & 0x3;
				if(reduced == 0)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;
				}
				else if(reduced == 1)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				else if(reduced == 2)
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;


					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;
				}
				else
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				return notes;
			}
		);

		var metalloPhone = new SoundPrefabs.Instruments.Metallophone(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(metalloPhonePatterns);
		metalloPhone.initializeWithVoices(4);
		m.createAndAddInstrument(metalloPhone, rsl,
			p =>
			{
				Sequencer.NoteInfo[] notes = new Sequencer.NoteInfo[1];
				notes[0].length = 0.5f + randomizer.random01() * 2;
				notes[0].velocity = -(15 + randomizer.random01() * 7);
				int octaveShift = (int)(randomizer.random01() * 1.1f) * 12;
				notes[0].pitch = EnvironmentTuning.pitchFromNote(EnvironmentTuning.A4 + DMinorFromA4[randomizer.randomRange(0, DMinorFromA4.Length - 1)]);
				notes[0].enabled = true;
				return notes;
			}
		);
	}

	void initializeMenuMusic(MusicPiece m)
	{
		RandomSequenceList rsl;
		var strings = new SoundPrefabs.Instruments.Strings(synth);
		rsl = new Blunt.Probability.Music.RandomSequenceList();
		rsl.addTimeSequences(mainStringPatterns);
		strings.initializeWithVoices(9);
		m.createAndAddInstrument(strings, rsl,
			p =>
			{
				Sequencer.NoteInfo[] notes;
				int reduced = p.bars & 0x3;
				if(reduced == 0)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;
				}
				else if(reduced == 1)
				{
					notes = new Sequencer.NoteInfo[3];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				else if(reduced == 2)
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
					notes[2].enabled = true;


					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;
				}
				else
				{
					notes = new Sequencer.NoteInfo[4];
					notes[0].length = 3.5f;
					notes[0].velocity = -(28 + randomizer.random01() * 3);
					notes[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 3);
					notes[0].enabled = true;

					notes[1].length = 3.5f;
					notes[1].velocity = -(28 + randomizer.random01() * 3);
					notes[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.ASharp, 4);
					notes[1].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
					notes[2].enabled = true;

					notes[2].length = 3.5f;
					notes[2].velocity = -(28 + randomizer.random01() * 3);
					notes[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
					notes[2].enabled = true;
				}
				return notes;
			}
		);

	}

	void onGameMusic()
	{
		menuMusic.stopPiece(0);
		mainMusic.playPiece(synth.getPlayHead().getDistanceToNextBeat());
		isInGame = true;
		stage = Stage.Start;
	}



	void endGameChordSequencerCallback(CallbackSequencer.Token t, PlayHead p)
	{
		if(!isInGame)
		{
			sequencer.playNotes(endingChordInstrument, endingChord);
		}
	}

	void pickupSequencerCallback(CallbackSequencer.Token t, PlayHead p)
	{
		
		Sequencer.NoteInfo[] note = new Sequencer.NoteInfo[1];
		note[0].pitch = EnvironmentTuning.pitchFromNote(EnvironmentTuning.A4 + DMinorFromA4[rewardMelody[rewardPosition]] - 12);
		note[0].velocity = -15;
		note[0].length = 0.0625f;
		note[0].enabled = true;
		rewardPosition++;

		sequencer.playNotes(rewardInstrument, note);

		if(rewardPosition == rewardMelody.Length)
		{
			note[0].length = 8;
			rewardPosition = 0;
		}
		else
		{
			t.callbackIn(rewardRhythm[rewardPosition]);
		}
	}

	void onGamePickup()
	{
		pickupSplash.playWaterEntry(0.8f);
		pickupToken.callbackIn(synth.getPlayHead().getDistanceToNextBeat());
	}

	void onGameFinished()
	{
		isInGame = false;
        mainMusic.stopPiece(0);
		menuMusic.stopPiece(0);
		waterMusic.stopPiece(0);

		utilityToken.callbackIn(synth.getPlayHead().getDistanceToNextBar(0, 2, 0, 0));
	}

	void onMenuMusic()
	{
		menuMusic.playPiece(0);
	}

	void SoundStart()
	{
		mixerChannel.setIsEnabled(true);
	}

	void onAudioLoad()
	{
		synth = new FMSynth();
		sequencer = new Sequencer(synth);
		cbseq = new CallbackSequencer(synth);
		mixerChannel = outputMixer.getChannel(mixerChannelToUse);
		mixerChannel.addStage(synth);
		menuMusic = new MusicPiece(sequencer, cbseq, synth);
		mainMusic = new MusicPiece(sequencer, cbseq, synth);
		waterMusic = new MusicPiece(sequencer, cbseq, synth);
		endingChordInstrument = new SoundPrefabs.Instruments.Strings(synth);
		endingChordInstrument.initializeWithVoices(6);

		rewardInstrument = new SoundPrefabs.Instruments.RewardPiece(synth);
		rewardInstrument.initializeWithVoices(7);

		pickupSplash = new SoundPrefabs.WaterSplash(synth);
		pickupSplash.initializeWithVoices(2);

		initializeMainMusic(mainMusic);
		initializeMenuMusic(menuMusic);
		initializeWaterMusic(waterMusic);
		utilityToken = cbseq.createCallbackToken(endGameChordSequencerCallback);
		pickupToken = cbseq.createCallbackToken(pickupSequencerCallback);
		quietToken = cbseq.createCallbackToken(quietSequencerCallback);
		puzzleSolvedToken = cbseq.createCallbackToken(puzzleSolvedCallback);
		endingChord = new Sequencer.NoteInfo[6];
		endingChord[0].enabled = true;
		endingChord[0].length = 3;
		endingChord[0].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 3);
		endingChord[0].velocity = -15;

		endingChord[1].enabled = true;
		endingChord[1].length = 3;
		endingChord[1].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 4);
		endingChord[1].velocity = -17;

		endingChord[2].enabled = true;
		endingChord[2].length = 3;
		endingChord[2].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.F, 4);
		endingChord[2].velocity = -15;


		endingChord[3].enabled = true;
		endingChord[3].length = 3;
		endingChord[3].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.A, 4);
		endingChord[3].velocity = -15;

		endingChord[4].enabled = true;
		endingChord[4].length = 3;
		endingChord[4].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.C, 5);
		endingChord[4].velocity = -17;

		endingChord[4].enabled = true;
		endingChord[4].length = 3;
		endingChord[4].pitch = EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.E, 5);
		endingChord[4].velocity = -17;
	}

	void soundUpdate()
	{
		if(isInGame)
		{
			if(!mainMusic.isCurrentlyPlaying() && !waterMusic.isCurrentlyPlaying())
			{
				if(isInWater)
				{
					onWaterEntry();
				}
				else
				{
					onWaterExit();
				}
			}
		}

	}
	void onWaterEntry()
	{
		isInWater = true;
		var distance = synth.getPlayHead().getDistanceToNextBar(0, 1, 0, 0);

		mainMusic.stopPiece(synth.getPlayHead().getDistanceToNextBeat());
		waterMusic.playPiece(synth.getPlayHead().getDistanceToNextBeat());
	}

	void onWaterExit()
	{
		isInWater = false;
		var distance = synth.getPlayHead().getDistanceToNextBar(2, 0, 0, 0);

		waterMusic.stopPiece(distance);
		mainMusic.resumePiece(synth.getPlayHead().getDistanceToNextBeat());
	}

	void quietSequencerCallback(CallbackSequencer.Token t, PlayHead p)
	{
		if(!isQuiet && stage == Stage.Start)
			stage = Stage.Quiet;
		
		isQuiet = !isQuiet;
		if(stage == Stage.Puzzle)
			isQuiet = false;
	}

	void onQuietEntry()
	{
		quietToken.callbackIn(synth.getPlayHead().getDistanceToNextBar(1, 0, 0, 0));
	}

	void onQuietExit()
	{
		quietToken.callbackIn(synth.getPlayHead().getDistanceToNextBar(0, 2, 0, 0));
	}

	void onPuzzleSolved()
	{
		puzzleSolvedToken.callbackIn(synth.getPlayHead().getDistanceToNextBar(0, 2, 0, 0));
	}

	void puzzleSolvedCallback(CallbackSequencer.Token t, PlayHead p)
	{
		stage = Stage.All;
	}

	void Start()
	{
		GameState.instance().onGameIsLoaded += SoundStart;
		GameState.instance().onAudioLoad += onAudioLoad;
		GameState.instance().onGameStart += onGameMusic;
		GameState.instance().onUpdate += soundUpdate;
		GameState.instance().onWaterEntry += onWaterEntry;
		GameState.instance().onWaterExit += onWaterExit;
		GameState.instance().onGameEnd += onGameFinished;
		GameState.instance().onGameMenu += onMenuMusic;
		GameState.instance().onPickup += onGamePickup;
		GameState.instance().onQuietEntry += onQuietEntry;
		GameState.instance().onQuietExit += onQuietExit;
		GameState.instance().onPuzzleEnter += () => stage = Stage.Puzzle;
		GameState.instance().onPuzzleSolved += onPuzzleSolved;

	}

}
