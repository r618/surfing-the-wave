﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt.UnityExt;

public class UIManager : MonoBehaviour 
{
	public bool skipMenus = false;
	public UnityEngine.UI.Text[] highscores;
	public UnityEngine.UI.InputField playerName;
	public UnityEngine.UI.Text scoreText;
	public UnityEngine.UI.Text gameOverMessage;

	private SoundPrefabs.MenuButton button;
	private Blunt.Synthesis.FMSynth synth;

	private int currentPlayerScore = 0;

	public GameObject startMenu, highscore, about, gameTitle;
	public FXComponentSynth fxSynth;
	private static UIManager internalInstance;

	public void onMenuStartButton()
	{
		button.stealOldestVoice().play(-6);
		GameState.instance().resumeGame();
		//Time.timeScale = GameState.instance().currentTimeScale;
		GameState.instance().onGameStart();
	}

	public UIManager instance()
	{
		return internalInstance;
	}

	public void onMenuAboutButton()
	{
		button.stealOldestVoice().play(-6);
		GameState.instance().pauseGame();
		gameTitle.SetActive(true);
		startMenu.SetActive(false);
		highscore.SetActive(false);
		about.SetActive(true);
	}

	public void onMenuExitButton()
	{
		button.stealOldestVoice().play(-6);
		GameState.instance().resumeGame();
		GameState.instance().onExit();
		this.delayedAction(() => Application.Quit(), 0.5f);
	}

	public void onStartMenu()
	{
		button.stealOldestVoice().play(-6);
		GameState.instance().pauseGame();
		about.SetActive(false);
		highscore.SetActive(false);
		startMenu.SetActive(true);
		gameTitle.SetActive(true);
	}

	public void onIngame()
	{
		GameState.instance().resumeGame();
		about.SetActive(false);
		gameTitle.SetActive(false);
		startMenu.SetActive(false);
		highscore.SetActive(false);
		scoreText.gameObject.SetActive(true);
	}

	public void onHighscore()
	{
		scoreText.gameObject.SetActive(false);
		GameState.instance().pauseGame();
		about.SetActive(false);
		gameTitle.SetActive(true);
		startMenu.SetActive(false);
		setScores();
		gameOverMessage.text = "Game over! You score was " + currentPlayerScore.ToString() + " points. ";
		gameOverMessage.text += "Submit your score below:";
		highscore.SetActive(true);

	}

	void Awake()
	{
		internalInstance = this;
	}

	void onAudioLoad()
	{
		synth = fxSynth.getSynthesizer();
		button = new SoundPrefabs.MenuButton(synth);
		button.initializeWithVoices(5);
	}

	void Start()
	{
		scoreText.gameObject.SetActive(false);
		if(skipMenus)
		{
			GameState.instance().onGameIsLoaded += () => GameState.instance().onGameStart();
		}
		else
		{
			GameState.instance().onGameIsLoaded += onStartMenu;
			GameState.instance().onGameIsLoaded += GameState.instance().onGameMenu;
			GameState.instance().onGameStart += onIngame;
			GameState.instance().onGameEnd += onHighscore;
			GameState.instance().onUpdate += onGameLoop;
			GameState.instance().onAudioIsLoaded += onAudioLoad;
		}

	}

	public void onGameLoop()
	{
		setCurrentPlayerScore((int)GameState.instance().gameTime);
	}

	public void setCurrentPlayerScore(int score)
	{
		currentPlayerScore = score;
		scoreText.text = "Score: " + score;
	}
	
	public void onUserSubmission()
	{
		if(playerName.text.Length > 0)
		{
			Blunt.CHighScore.submitScore(new Blunt.CHighScore.PlayerEntry(playerName.text, currentPlayerScore));
			setScores();
			playerName.gameObject.SetActive(true);
		}
		
	}
	
	private void setScores()
	{
		var scores = Blunt.CHighScore.getScores ();
		for(int i = 0; i < highscores.Length; ++i)
		{
			if(i < scores.Length)
			{
				highscores[i].text = "#" + (highscores.Length - i).ToString() + " - " + scores[i].playerName + " (" + scores[i].score + ")";
			}
			else
			{
				highscores[i].text = "#" + (highscores.Length - i).ToString();
			}
		}
		
	}
}
