﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Blunt;
using Blunt.Synthesis;
using Blunt.Sound;

namespace SoundPrefabs
{
	using System;
	using Synth = Blunt.Synthesis.FMSynth;

	public abstract class SoundPrefab : VoiceBuffer<Synth.Voice>
	{
		public SoundPrefab(Synth s) : base(s) { }
		public abstract void initializeWithVoices(int voices);
	}

	namespace Instruments
	{

		class Strings : SoundPrefab
		{
			public Strings(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;
						op = voice.createAndAddRandomOffset();
						op.volume = Mathf.PI * 0.5f;
						op.isModulator = true;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1500, 3000, -12, 1000);

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1500, 3000, -12, 1000);
						op.pitchRelation = 2.005f;
						op.mix = 0.5f;


						op = voice.createAndAddOperator();
						op.isModulator = true;
						op.volume = 0.2f;
						op.envelope.setADSR(1500, 1000, -12, 10000);
						op = voice.createAndAddOperator();
						op.envelope.setADSR(1500, 3000, -12, 1000);
						op.pitchRelation = 0.497f;
						op.mix = 0.3333333333f;
					}
				);
			}
		}

		class Arpeggio : SoundPrefab
		{
			public Arpeggio(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(500, 0, 0, 100);
						op.isModulator = true;
						op.volume = 0.1f;
						op.pitchRelation = 4;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1, 100, -6, 50);

						op = voice.createAndAddOperator();
						op.envelope.setADSR(100, 100, -6, 50);
						op.mix = 0.5f;
						op.pitchRelation = 2f;
					}
				);
			}
		}


		class HiHat : SoundPrefab
		{
			public HiHat(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.kind = FMSynth.Voice.Operator.Kind.Noise;

						op.envelope.setADSR(0, 50, -96, 10);
						op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 4000, 0.5f);
					}
				);
			}
		}

		class LFOChords : SoundPrefab
		{
			public LFOChords(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 0, 0, 100000);
						op.isModulator = true;
						op.volume = 0.3f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 1000, -64, 1000);
						op.kind = Synth.Voice.Operator.Kind.Sine;
						op.op = Synth.Voice.Operator.Operation.Additive;
						op.volume = 1f;


						op = voice.createAndAddOperator();
						op.envelope.setADSR(1000, 0, 0, 10000);
						op.isModulator = true;
						op.isFixed = true;
						op.omega = EnvironmentTuning.bpmFreq * 2;
						op.volume = 4f;
						op.phaseOffset = Mathf.PI * 1.5f;

						op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 50, 5);
					}
				);
			}
		}

		class FMBass : SoundPrefab
		{
			public FMBass(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 0, 0, 100000);
						op.isModulator = true;
						op.pitchRelation = 0.75f;
						op.volume = 0.1f;
						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 0, 0, 100000);
						op.isModulator = true;
						op.pitchRelation = 1.001f;
						op.volume = 0.25f;
						op.mix = 0.9f;
						op = voice.createAndAddOperator();
						op.envelope.setADSR(20, 3500, -64, 100);
						op.kind = Synth.Voice.Operator.Kind.Sine;
						op.op = Synth.Voice.Operator.Operation.Additive;
						op.volume = 1f;

						op = voice.createAndAddRandomOffset();
						op.volume = 0.2f;
						op.isModulator = true;

						op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 90, 3);
						op.mix = 0.7f;
						op.modulationDepth = 0.7f;
						op.envelope.setADSR(8000, 5000, -24, 100000);
					}
				);
			}
		}


		class Metallophone : SoundPrefab
		{
			public Metallophone(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.isModulator = true;
						op.pitchRelation = 1f;
						op.envelope.setADSR(100, 0, 0, 100000);
						op.volume = 0.01f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1, 900, -24, 3000);
						op.pitchRelation = 0.9975f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1, 900, -24, 3000);
						op.pitchRelation = 2.0025f;
						op.mix = 0.5f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 0, 0, 5000);
						op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
						op.mix = 0.01f;
						op.isFixed = true;
						op.pitchRelation = 2;
						op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.D, 5);

					}
				);
			}
		}

		class RewardPiece : SoundPrefab
		{
			public RewardPiece(FMSynth s) : base(s) { }


			public override void initializeWithVoices(int voices)
			{
				initialize(voices,
					voice =>
					{
						Synth.Voice.Operator op;

						op = voice.createAndAddOperator();
						op.isModulator = true;
						op.pitchRelation = 1f;
						op.envelope.setADSR(100, 0, 0, 100000);
						op.volume = 0.15f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1, 900, -24, 2000);
						op.pitchRelation = 0.9975f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(1, 900, -24, 2000);
						op.pitchRelation = 2.0025f;
						op.mix = 0.5f;

						op = voice.createAndAddOperator();
						op.envelope.setADSR(0, 0, 0, 5000);
						op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
						op.mix = 0.05f;
						op.isFixed = true;
						op.pitchRelation = 2;

						op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.D, 5);

						op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 3000, 4);
						op.modulationDepth = 0.7f;
						op.envelope.setADSR(5000, 500, -6, 10000);

					}
				);
			}
		}

	}


	public class RotatingWindResitance : SoundPrefab, Synth.SequenceCallback
	{
		int numObjects;
		float angularFrequency;
		public RotatingWindResitance(Synth s) : base(s)
		{
			
		}

		public void setNumResistingObjects(int objects)
		{
			numObjects = objects;
			apply();
		}

		public void setRotatingFrequency(float frequency)
		{
			angularFrequency = frequency;
			apply();
		}

		public void setProperties(float frequency, int objects)
		{
			angularFrequency = frequency;
			numObjects = objects;
			apply();
		}

		private void apply()
		{
			float omega = numObjects * angularFrequency * EnvironmentTuning.hzToRads;
			float filterAddition = Mathf.Atan(angularFrequency) * 1.3f;
            foreachVoice(
				v =>
				{
					v.operators[2].omega = omega;
					v.operators[7].omega = omega * 1.5f; // amplitude modulation, 1/3 as fast to simulate front and back of rotating object
					v.operators[3].parameterSetGoal(filterAddition, 10);
					v.operators[5].omega = omega * 3;
					v.operators[6].omega = omega * 3;
				}
			);
		}

		public void onSequencing(Synth s, Blunt.Synthesis.PlayHead p)
		{

		}

		public void setVolume(float volume, float transitionTime)
		{
			foreachVoice(voice => voice.operators[1].parameterSetGoal(volume, transitionTime));
		}

		public override void initializeWithVoices(int numVoices)
		{
			initialize(numVoices,
				voice =>
				{
					Synth.Voice.Operator op;
					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;

					op = voice.createAndAddParameter(0f);
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;



					op = voice.createAndAddOperator();

					op.isFixed = true;
					op.isModulator = true;
					//op.omega = EnvironmentTuning.bpmFreq * angularFrequency;
					op.phaseOffset = Mathf.PI * 1.5f;
					op.omega = numObjects * angularFrequency * EnvironmentTuning.hzToRads;
					op.volume = 0.7f;

					op = voice.createAndAddParameter(0);
					op.volume = 4;
					op.isModulator = true;
					op.mix = 0.5f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 50, 3);

					op = voice.createAndAddOperator();
					op.isModulator = true;
					op.isFixed = true;
					op.volume = 0.7f;

					op = voice.createAndAddOperator();
					op.mix = 0.0075f;
					op.isFixed = true;

					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.isModulator = false;
					op.mix = 0.1f;
					op.phaseOffset = Mathf.PI * 1.5f;
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;

				}
			);
		}

	}

	public class Kick : SoundPrefab
	{
		public Kick(Synth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					// reeverb room noise
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.isModulator = false;
					op.envelope.setADSR(1, 200, -48, 200);
					op.mix = 1f;
					op.volume = 2.0f;

					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op.volume = 0.15f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 63, 8);



					// creates the pitch down effect on kicks
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.phaseOffset = Mathf.PI;
					op.envelope.setADSR(1, 800, -96, 1000);
					op.omega = EnvironmentTuning.bpmFreq * 0.25f;
					op.isModulator = true;
					op.volume = 15;


					//op.volume = 0.1f;
					// the main operator
					op = voice.createAndAddOperator();
					op.kind = Synth.Voice.Operator.Kind.Sine;
					op.op = Synth.Voice.Operator.Operation.Additive;
					op.volume = 2.0f;
					op.envelope.setADSR(0, 400, -96, 1000);
					op.mix = 0.6f;
					op.pitchRelation = 0.125f;

					// boomy filter
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 80, 12);
					op.mix = 0.5f;
					// base pitch
					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);

		}
	}
	public class Snare : SoundPrefab
	{
		public Snare(Synth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.omega = EnvironmentTuning.hzToRads * 150f;
					op.isFixed = true;
					op.envelope.setADSR(0, 350, -64, 200);

					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.envelope.setADSR(0, 300, -6, 0);
					op.omega = EnvironmentTuning.hzToRads * 33f;
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;

					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.volume = 1.5f;
					op.isModulator = true;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 200, 8);
					op.envelope.setADSR(2000, 200, -24, 200);
					op.modulationDepth = 0.5f;
					op.isModulator = false;

					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.envelope.setADSR(0, 250, -96, 0);
					op.mix = 0.5f;
					op.op = FMSynth.Voice.Operator.Operation.Additive;

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 150, 5);
					op.modulationDepth = -0.8f;
					op.envelope.setADSR(60, 300, -64, 0);
					op.mix = 0.80f;

					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op.volume = 2;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 250, 5);
					op.mix = 0.65f;
					// base pitch
					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);

		}

	}

	public class BlowingWind : SoundPrefab
	{
		private List<Synth.Voice.Operator> blowingParameter;
		private List<Synth.Voice.Operator> volume;
		public BlowingWind(Synth s) : base(s)
		{
			blowingParameter = new List<FMSynth.Voice.Operator>();
			volume = new List<FMSynth.Voice.Operator>();
		}
		public override Synth.Voice stealOldestVoice()
		{
			var v = voices.pop();
			// reset the parameter...

			voices.push(v);
			return v;

		}
		public void setBlowingIntensity(float intensity, float milisecondsToTransition)
		{
			foreach(var b in blowingParameter)
			{
				b.parameterSetGoal(intensity, milisecondsToTransition);
			}
			foreach(var v in volume)
			{
				v.parameterSetGoal(Mathf.Clamp01(0.5f * intensity * intensity * intensity + 0.1f), 10);
			
			}
		}



		public override void initializeWithVoices(int voices)
		{
			blowingParameter.Clear();
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.envelope.setADSR(11000, 200, -5, 2000);
					op.volume = 2f;
					op = voice.createAndAddParameter(0);
					op.volume = 3.5f;
					op.isModulator = true;
					blowingParameter.Add(op);
					// this makes the wind 'open' up as air resistance increases
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 10, 5);
					// this just adds a resonant peak, making the wind seem hollow
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 300, 7);
					op.mix = 0.4f;
					op = voice.createAndAddParameter(0);
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
					volume.Add(op);
				}
			);

		}

	}

	public class SonicBoom : SoundPrefab
	{
		public SonicBoom(Synth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					// reeverb room noise
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.isModulator = false;
					op.envelope.setADSR(1, 2000, -12, 1000);
					op.mix = 1f;
					op.volume = 2.0f;

					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op.volume = 0.15f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 55, 7);

					// base pitch
					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);

		}
	}

	public class Metronome : SoundPrefab
	{
		public Metronome(Synth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.isModulator = true;
					op.envelope.setADSR(0, 0, 0, 1000);

					// modulator
					op = voice.createAndAddOperator();
					op.envelope.setADSR(1, 20, -96, 10);

					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);
		}

	}

	public class WaterSplash : SoundPrefab
	{
		public WaterSplash(Synth s) : base(s) { }

		public void playWaterEntry(float velocity)
		{
			var voice = stealOldestVoice();
			velocity = Mathf.Clamp01(velocity);

			voice.stop();
			voice.setPitch(22f + velocity * 20);
			voice.volume = velocity * velocity;

			voice.play();
		}

		public void playWaterExit(float velocity)
		{
			var voice = stealOldestVoice();
			velocity = Mathf.Clamp01(velocity);

			voice.stop();
			voice.setPitch(22f + velocity * 5);
			voice.volume = velocity;

			voice.play();
		}

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					// reeverb room noise
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.isModulator = false;
					op.envelope.setADSR(1, 2000, -48, 200);
					op.mix = 1f;
					op.volume = 2.0f;

					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op.volume = 1f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 63, 8);

					op.mix = 0.6f;

					// creates the pitch down effect on kicks
					op = voice.createAndAddOperator();
					op.isFixed = false;
					op.phaseOffset = Mathf.PI;
					op.envelope.setADSR(1, 800, -96, 1000);
					op.omega = EnvironmentTuning.bpmFreq * 0.25f;
					op.isModulator = true;
					op.volume = 15;
					op.pitchRelation = 0.001f;

					//op.volume = 0.1f;
					// the main operator
					op = voice.createAndAddOperator();
					op.kind = Synth.Voice.Operator.Kind.Sine;
					op.op = Synth.Voice.Operator.Operation.Additive;
					op.volume = 2.0f;
					op.envelope.setADSR(0, 4000, -96, 1000);
					op.isModulator = true;
					op.pitchRelation = 0.125f;

					// boomy filter
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 80, 8);
					op.mix = 0.7f;
					// base pitch
					voice.setPitch(EnvironmentTuning.A4, 0);

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 50, 4);
					op.modulationDepth = 0.6f;
					op.envelope.setADSR(3000, 1000, -24, 10000);

					op.mix = 0.5f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 100, 4);
					op.modulationDepth = -4f;
					op.envelope.setADSR(3000, 1000, -24, 10000);

					op.mix = 0.4f;

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.HP, 40, 1);
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 8000, 0.1f);
				}
			);
		}

	}

	public class RewardSound : SoundPrefab
	{
		public RewardSound(Synth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;


					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.A, 3);
					op.isModulator = true;
					op.volume = 2;
					op.envelope.setADSR(100000, 0, 0, 1000);

					// modulator
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.A, 1);
					op.isModulator = true;
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;

					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.A, 1) * 0.99f;
					op.isModulator = true;
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
					op.mix = 0.5f;
					// carrier
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.A, 3);
					op.envelope.setADSR(10, 40000, -96, 1000);

					/*op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 220, 5);
					op.modulationDepth = 1.0f;
					op.envelope.setADSR(10000, 1000, -96, 1000);
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.BP, 220, 5);
					op.modulationDepth = 1.0f;
					op.envelope.setADSR(10000, 1000, -96, 1000);*/
					// base pitch
					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);

		}
	}

	public class ShepardTone : SoundPrefab, Synth.SequenceCallback
	{

		private Synth.Voice currentVoice;
		private Synth.Voice droneVoice;
		VoiceBuffer<Synth.Voice> drones;
		int tones;
		float power2Cap;
		float scale;
		float freq;
		float pitch;
		private ToneData[] tinfo;
		/// <summary>
		/// We calculate the new values in the GUI loop, and assign them
		/// at next sequencing.
		/// </summary>
		struct ToneData
		{
			public float volume, omega;
		}

		/// <summary>
		/// nTones is how many octaves will play at the same time. the more, the higher they will go as well.
		/// for baseFreequency = 1, the shepard tone will go through a single full range in progress(0..1).
		/// higher frequencies causes it to faster.
		/// </summary>
		/// <param name="s"></param>
		/// <param name="nTones"></param>
		/// <param name="numOctavesToRise"></param>
		/// <param name="baseFrequency"></param>
		public ShepardTone(Synth s, int nTones, float baseFrequency)
			: base(s)
		{
			s.addSequencer(this);
			tones = nTones;
			power2Cap = Mathf.Pow(2, tones);
			freq = baseFrequency;
			tinfo = new ToneData[tones << 1];
			drones = new VoiceBuffer<FMSynth.Voice>(s);
		}

		~ShepardTone()
		{
			((Synth)hostSynth).removeSequencer(this);
		}

		public void onSequencing(Synth s, PlayHead p)
		{
			if(currentVoice != null)
			{
				ToneData[] tempData = tinfo;
				int entry = 0, idx = 0;
				foreach(var o in currentVoice.operators)
				{
					if(entry < (tones << 1))
					{
						
						o.volume = tempData[idx].volume;
						o.omega = tempData[idx].omega;
						idx++;
					}
					entry++;
				}

			}
		}

		public void play(float volume, float basePitch)
		{
			currentVoice = stealOldestVoice();
			droneVoice = drones.stealOldestVoice();
			currentVoice.stop();
			droneVoice.stop();
			pitch = basePitch;
			setProgress(0);
			currentVoice.play(volume);
			droneVoice.play(volume);
		}

		public void release()
		{
			currentVoice.release();
			droneVoice.release();
			droneVoice = null;
			currentVoice = null;
		}
		/// <summary>
		/// Non-audio rate modulations, be careful with aliasing at high speeds.
		/// </summary>
		/// <param name="progress"></param>
		public void setProgress(float progress)
		{
			if(currentVoice != null)
			{
				ToneData[] tempData = new ToneData[tones << 1];
				var rads = EnvironmentTuning.hzToRads;
				int entry = 0;
				int idx = 0;
				foreach(var o in currentVoice.operators)
				{
					if(entry < (tones << 1))
					{ 
						// scale the tone.
						float scaleProgress = (o.pitchRelation + progress * freq * tones) % (float)tones;
						// get the normalized progress:
						float normProgress = scaleProgress / tones;
						if(!o.isModulator)
						{
							// the volume is the raised cosine window, others can be used like Hann or something.
							float volume = (1 - Mathf.Cos(normProgress * Mathf.PI * 2)) * 0.5f;
							// create a gaussian n^a  distribution
							tempData[idx].volume = Mathf.Pow(volume, 10);
						}
						else
						{
							tempData[idx].volume = o.volume;
						}

						// get the exponential progress, for the pitch
						float p = Mathf.Pow(2, scaleProgress);
						// convert to radians
						tempData[idx].omega = p * pitch * 2 * Mathf.PI * EnvironmentTuning.deltaT;
						idx++;
					}
					entry++;
				}
				tinfo = tempData;
			}

		}

		public override void initializeWithVoices(int voices)
		{
			int overtone = 1;

			// create the shepard system
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;

					for(int i = 0; i < tones; ++i)
					{
						op = voice.createAndAddOperator();
						op.pitchRelation = overtone;
						op.isModulator = true;
						op.volume = 0.5f;
						op.envelope.setADSR(0, 0, 0, 10000);

						op = voice.createAndAddOperator();
						op.pitchRelation = overtone;
						op.mix = 1.0f / (1 + i);
						op.envelope.setADSR(1000, 1, 0, 1000);

						overtone += 1; 

					}


					// add a funny, random lfo resonant filter
					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.isModulator = true;
					op.omega = EnvironmentTuning.hzToRads * freq * 0.33f;
					op.envelope.setADSR(100, 0, 0, 10000);
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 100, 5);
					op.mix = 0.6f;

					voice.setPitch(EnvironmentTuning.A4, 0);
				}
			);
			// create the machine drone:

			drones.initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					// add a drone. firstly, the bass modulator fm:
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = 0.5f * EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.C, 2);
					op.volume = MathExt.dbToFraction(-9);
					op.isModulator = true;
					op.envelope.setADSR(10, 0, 0, 1000);
					// then the carrier:
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.radiansFromComposite(EnvironmentTuning.Pitch.C, 2);
					op.envelope.setADSR(100, 0, 0, 1000);

					// and now some tremolo, to make it sound like a machine.
					// first create a modulator
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = 1 * EnvironmentTuning.hzToRads;
					op.isModulator = true;
					op.envelope.setADSR(4000, 0, 0, 500);
					// then create a stateless carrier (0 hz)
					// fixed as cos(0 + ø), so we can emulate a machine starting and stopping
					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.omega = EnvironmentTuning.hzToRads * 0.25f;
					op.phaseOffset = 0.5f * Mathf.PI;
					op.op = FMSynth.Voice.Operator.Operation.Multiplicative;
					op.envelope.setADSR(800, 0, 0, 10000);
					op.volume = 0.5f; // (the final relative volume)
					voice.setPitch(EnvironmentTuning.A4, 0);

					// AAAND a highpass.
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.HP, 50, 1);
				}
			);
		}
	}
	/// <summary>
	/// Landingsound can modulate the next playable sound in a queue,
	/// but never the currently playing one. Keep this in mind!
	/// </summary>
	class LandSound : SoundPrefab
	{
		public LandSound(FMSynth s) : base(s) { }

		Synth.Voice current;


		public void play(float volume, float intensity)
		{
			current = stealOldestVoice();
			current.stop();
			intensity = Mathf.Clamp01(intensity);
			current.operators[0].envelope.setADSR(1, 50 + intensity * 900, -120, 100);
			current.operators[0].volume = 0.11f + intensity * 0.5f;
			current.operators[1].parameterSetValue(intensity);
			current.play(volume);

		}

		public override void initializeWithVoices(int numVoices)
		{
			initialize(numVoices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.isModulator = false;
					op.envelope.setADSR(1, 2000, -96, 1000);
					op.mix = 1f;
					op.volume = 2.0f;

					op = voice.createAndAddParameter(0f);
					op.isModulator = true;
					op.volume = 0.3f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 72, 3);
				}
			);
			current = voices.newest();
		}

	}

	class BallLandSound : SoundPrefab
	{
		public BallLandSound(FMSynth s) : base(s) { }

		Synth.Voice current;

		public void play(float volume, float intensity)
		{
			current = stealOldestVoice();
			current.stop();
			intensity = Mathf.Clamp01(intensity);
			current.operators[0].envelope.setADSR(1, 250 + intensity * 1200, -120, 100);
			current.operators[0].volume = 0.11f + intensity * 0.5f;
			current.operators[1].parameterSetValue(intensity);
			current.play(volume);

		}

		public override void initializeWithVoices(int numVoices)
		{
			initialize(numVoices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.isFixed = true;
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.isModulator = false;
					op.envelope.setADSR(1, 2000, -96, 1000);
					op.mix = 1f;
					op.volume = 2.0f;

					op = voice.createAndAddParameter(0f);
					op.isModulator = true;
					op.volume = 0.3f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 45, 6f);
				}
			);
			current = voices.newest();
		}

	}

	class JumpSound : SoundPrefab
	{
		public JumpSound(FMSynth s) : base(s) { }


		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;

					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.envelope.setADSR(10, 100, -96, 100);

					op = voice.createAndAddRandomOffset();
					op.isModulator = true;
					op.volume = 0.3f;
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 80, 5);
					op.modulationDepth = 0.33f;
					op.envelope.setADSR(250, 5000, -24, 1000);
				}
			);
		}
	}


	class AbruptAbility : SoundPrefab
	{

		public AbruptAbility(FMSynth s) : base(s) { }

		public override void initializeWithVoices(int numVoices)
		{
			initialize(numVoices,
				voice =>
				{
					Synth.Voice.Operator op;
					op = voice.createAndAddOperator();
					op.volume = 0.3f;
					op.envelope.setADSR(0, 0, 0, 100000);
					op.isModulator = true;

					op = voice.createAndAddOperator();
					op.envelope.setADSR(5, 500, -96, 500);

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 100, 2);
					op.mix = 0.3f;
					op.modulationDepth = 0.5f;
					op.envelope.setADSR(1000, 1000, -12, 2000);

					voice.setPitch(EnvironmentTuning.noteFromComposite(EnvironmentTuning.Pitch.A, 3));
				}
			);
		}

	}

	class RisingAbility : SoundPrefab
	{
		public RisingAbility(FMSynth s) : base(s) { }


		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					op = voice.createAndAddOperator();
					op.volume = 0.3f;
					op.envelope.setADSR(0, 0, 0, 100000);
					op.isModulator = true;

					op = voice.createAndAddOperator();
					op.envelope.setADSR(1500, 10, -96, 100);

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 100, 2);
					op.mix = 0.3f;
					op.modulationDepth = 0.5f;
					op.envelope.setADSR(3000, 1000, -12, 100);

					voice.setPitch(EnvironmentTuning.noteFromComposite(EnvironmentTuning.Pitch.A, 3));
				}
			);
		}
	}

	class MenuButton : SoundPrefab
	{
		public MenuButton(FMSynth s) : base(s) { }


		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					op = voice.createAndAddOperator();
					op.isModulator = true;
					op.pitchRelation = 0.75f;
					op.volume = 0.3f;

					op = voice.createAndAddOperator();
					op.envelope.setADSR(1, 5, -96, 5);

					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.envelope.setADSR(1, 20, -95, 5);
					op.mix = 0.5f;

					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 80, 5);
					op.mix = 0.99f;
					voice.setPitch(EnvironmentTuning.noteFromComposite(EnvironmentTuning.Pitch.A, 3));
				}
			);
		}
	}

	class SimpleButtonClick : SoundPrefab
	{
		public SimpleButtonClick(FMSynth s) : base(s) { }

		public override void initializeWithVoices(int voices)
		{
			initialize(voices,
				voice =>
				{
					Synth.Voice.Operator op;
					op = voice.createAndAddOperator();
					op.kind = FMSynth.Voice.Operator.Kind.Noise;
					op.envelope.setADSR(0, 1000, -96, 10);
					op = voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 200, 5);
					op.modulationDepth = -4;
					op.envelope.setADSR(100, 0, 0, 10000);
				}
			);
		}
	}

}
