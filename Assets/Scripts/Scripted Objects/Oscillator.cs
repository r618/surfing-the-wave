﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class Oscillator : MonoBehaviour
{

	public float volume;
	public float startFrequency;
	public float stopFrequency;
	public float percentage;

	private float phase;
	private float omega;
	public  float sampleRate = 1;
	// Use this for initialization
	void Start ()
	{
		sampleRate = 1;
    }

	void OnAudioFilterRead(float[] data, int channels)
	{
		float currentFrequency = startFrequency + percentage * 0.01f * (stopFrequency - startFrequency);
		omega = 2 * Mathf.PI * currentFrequency / sampleRate;

		for(int i = 0; i < data.Length; i += channels)
		{
			data[i] = data[i + 1] = volume * Mathf.Sin(phase);
			phase += omega;
			if(phase > 2 * Mathf.PI)
				phase -= 2 * Mathf.PI;
		}
	}

	void Update()
	{
		sampleRate = AudioSettings.outputSampleRate;
	}
}
