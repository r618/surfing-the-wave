﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt.Synthesis;
using Blunt.Sound;
public class SelfSounds : MonoBehaviour
{
	FMSynth synth;

	SoundPrefabs.BlowingWind wind;
	SoundPrefabs.SoundPrefab pickupSound;
	SoundPrefabs.AbruptAbility abrupt;
	SoundPrefabs.RisingAbility rising;
	SoundPrefabs.JumpSound jump;
	SoundPrefabs.LandSound land;
	SoundPrefabs.WaterSplash splash;

	public Mixer mixer;
	public float selfActivatedSoundLevel = -15f;
	public float velocitySmoothing = 0.9f;
	Mixer.MixerChannel channel;
	public string channelToUse = "Self Sounds";
	FMSynth.Voice currentVoice;

	private float oldVelocity;

	void onAudioLoad()
	{
		synth = new FMSynth();
		synth.setLatency(4096);
		synth.setCutThreshold(-200);

		wind = new SoundPrefabs.BlowingWind(synth);
		wind.initializeWithVoices(1);

		pickupSound = new SoundPrefabs.Metronome(synth);
		pickupSound.initializeWithVoices(2);

		jump = new SoundPrefabs.JumpSound(synth);
		jump.initializeWithVoices(4);

		land = new SoundPrefabs.LandSound(synth);
		land.initializeWithVoices(4);

		abrupt = new SoundPrefabs.AbruptAbility(synth);
		abrupt.initializeWithVoices(8);
		abrupt.foreachVoice(v => v.setPitch(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 2)));
		rising = new SoundPrefabs.RisingAbility(synth);
		rising.initializeWithVoices(8);
		rising.foreachVoice(v => v.setPitch(EnvironmentTuning.pitchFromComposite(EnvironmentTuning.Pitch.D, 2)));
		splash = new SoundPrefabs.WaterSplash(synth);
		splash.initializeWithVoices(3);

		channel = mixer.getChannel(channelToUse);
		channel.addStage(synth);
	}

	void Start ()
	{
		GameState.instance().onAudioLoad += onAudioLoad;
		GameState.instance().onGameIsLoaded += onSoundStart;
		GameState.instance().onAirborne += startWindySound;
		GameState.instance().onGrounded += stopWindySound;
		GameState.instance().onUpdate += soundUpdate;
		GameState.instance().onJump += () => jump.stealOldestVoice().play(-9);
		GameState.instance().onGrounded += () =>
			{
				var vel = 0.35f *  GameState.instance().playerVelocityInLastFixedUpdate.magnitude / GameState.instance().mainPlayer.maximumVelocity;
				land.play(0, vel * vel);
                print("last velocity: " + vel + ", vel = " + GameState.instance().playerVelocityInLastFixedUpdate.magnitude + " mvol = " + GameState.instance().mainPlayer.maximumVelocity);
			};
		GameState.instance().onWaterEntry += () =>
		{
			var airRes = Mathf.Atan(GameState.instance().playerVelocityInLastFixedUpdate.magnitude * 0.5f);
			print("watersplash: " + 0.6f * airRes * airRes);
            splash.playWaterEntry(0.6f * airRes * airRes);
		};
		GameState.instance().onWaterExit += () =>
		{
			splash.playWaterExit(0.2f * GameState.instance().playerVelocityInLastFixedUpdate.magnitude / GameState.instance().mainPlayer.maximumVelocity);
		};
	}
	void startWindySound()
	{
		/*currentVoice = wind.stealOldestVoice();
		//wind.setBlowingIntensity(0, 0);
		currentVoice.play(1f);*/
	}

	void stopWindySound()
	{
		/*wind.foreachVoice(v => v.release());
		currentVoice = null;*/
	}
	void onSoundStart()
	{
		channel.setIsEnabled(true);
		wind.stealOldestVoice().play(-24);
	}


	void soundUpdate ()
	{
		if(GameState.instance().numAbilitiesPickedUp > 0)
		{
			if(Input.GetKeyDown(KeyCode.O))
			{
				rising.stealOldestVoice().play(selfActivatedSoundLevel);
			}
			if(Input.GetKeyUp(KeyCode.O))
			{
				rising.getCurrent().release();
			}
		}
		if(GameState.instance().numAbilitiesPickedUp > 1)
		{
			if(Input.GetKeyDown(KeyCode.P))
			{
				abrupt.stealOldestVoice().play(selfActivatedSoundLevel);
			}
			if(Input.GetKeyUp(KeyCode.P))
			{
				abrupt.getCurrent().release();
			}
		}

		// apply a smooth soft clipping curve
		var res = GameState.instance().airResistance;
        float newVelocity = Mathf.Atan(res * res *  3);
		// along with a filter..
		oldVelocity = newVelocity + velocitySmoothing * (oldVelocity - newVelocity);
		wind.setBlowingIntensity(oldVelocity, 150);
	}
}
