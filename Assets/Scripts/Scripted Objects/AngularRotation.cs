﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class AngularRotation : MonoBehaviour
{
	public Vector3 rotation;
	public float frequencyInHz;
	public float rotationalSpeed = 1.0f;

	private Transform tsf;

	// Use this for initialization
	void Start ()
	{
		tsf = GetComponent<Transform>();
	}

	public void setSpeed(float speed)
	{
		rotationalSpeed = speed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(rotationalSpeed != 0.0f)
			tsf.Rotate(rotationalSpeed * rotation * Time.deltaTime * frequencyInHz);
	}
}
