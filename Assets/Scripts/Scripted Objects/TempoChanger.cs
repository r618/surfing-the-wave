﻿using UnityEngine;
using System.Collections;

public class TempoChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.B))
		{
			Blunt.Synthesis.EnvironmentTuning.samplesPerBeat >>= 1;
			Blunt.Synthesis.EnvironmentTuning.updateFields();
		}
	}
}
