﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt.Synthesis;

public class BallSound : MonoBehaviour
{

	public FXComponentSynth fxSynth;
	private FMSynth synth;
	private SoundPrefabs.BallLandSound landSound;

	private Vector3 lastVelocity;
	private Vector3 newVelocity;
	private Rigidbody2D body;
	bool isGameLoaded = false;
	// Use this for initialization
	void Start ()
	{
		GameState.instance().onAudioIsLoaded += onAudioLoad;
		GameState.instance().onPrePhysics += onPrePhysics;
		GameState.instance().onGameIsLoaded += () => isGameLoaded = true;
		body = GetComponent<Rigidbody2D>();

	}

	void onAudioLoad()
	{
		synth = fxSynth.getSynthesizer();
		landSound = new SoundPrefabs.BallLandSound(synth);
		landSound.initializeWithVoices(5);
	}
	
	void onPrePhysics()
	{
		lastVelocity = newVelocity;
		if(body != null)
		{
			newVelocity = body.velocity;
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(isGameLoaded)
		{
			print("bvel = " + lastVelocity.magnitude);
			var vel = Mathf.Atan(lastVelocity.magnitude) * 0.7f;
			print("vel: " + vel);
			landSound.play(vel * vel + 0.1f, vel);
		}

	}

	// Update is called once per frame
	void Update ()
	{
	
	}
}
