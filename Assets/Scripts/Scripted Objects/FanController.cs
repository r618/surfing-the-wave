﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt;
using Blunt.Synthesis;
using Blunt.Sound;

public class FanController : MonoBehaviour
{
	public FXComponentSynth componentSynth;
	private FMSynth synth;
	private AngularRotation ar;
	private SoundPrefabs.RotatingWindResitance windEmitter;
	private Transform tsf;
	private Transform player;
	public float currentSpeed;
	public float fanSpeed = 2.5f;
	private float targetSpeed = 0;
	private float speedVelocity;
	// Use this for initialization

	public void startFan()
	{
		targetSpeed = fanSpeed;
	}
	public void stopFan()
	{
		targetSpeed = 0.0f;
	}
	private void loadAudio()
	{
		synth = componentSynth.getSynthesizer();
		windEmitter = new SoundPrefabs.RotatingWindResitance(synth);
		windEmitter.setNumResistingObjects(8);
		windEmitter.setRotatingFrequency(120 / 360f);
		windEmitter.initializeWithVoices(1);
		windEmitter.setVolume(0, 10);
		windEmitter.foreachVoice(v => v.dontKillOnSilence = true);
	}

	void onGameStart()
	{
		tsf = GetComponent<Transform>();
		ar = GetComponent<AngularRotation>();
		player = GameState.instance().mainPlayer.GetComponent<Transform>();
		currentSpeed = 0.0f;
		targetSpeed = 0;
		speedVelocity = 0.0f;
    }

	void Start ()
	{
		GameState.instance().onAudioIsLoaded += loadAudio;
		GameState.instance().onGameStart += () => windEmitter.stealOldestVoice().play(0f);
		GameState.instance().onGameEnd += () => windEmitter.getCurrent().release();
		GameState.instance().onUpdate += soundUpdate;
		GameState.instance().onGameIsLoaded += onGameStart;
	}
	
	void setFanSpeed(float speed)
	{
		ar.setSpeed(speed);
		windEmitter.setRotatingFrequency(speed * 120 / 360f);
	}

	// Update is called once per frame
	void soundUpdate ()
	{
		currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedVelocity, 7f);
		setFanSpeed(currentSpeed);

		var dist = Mathf.Exp(-(player.position - tsf.position ).magnitude / 2);
		if(currentSpeed > 0)
		   windEmitter.setVolume(Mathf.Min(dist  + 0.10f, 1.0f), 1);
	}
}
