﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

#define BLUNT_DEBUG

using UnityEngine;
using System.Collections;

using DSPType = System.Single;


public class WalkableWaveform : Waveform
{

	public class PhysicalInteraction
	{
		public Vector3 displacement = new Vector3();
		/// <summary>
		/// denotes the horizontal push direction (-1 pushes completely right, +1 pushes completely left).
		/// </summary>
		public float horizontalAngle;
		public bool collisionOccured;
	}
	/// <summary>
	/// Performs a collision (on the waveform) calculation on the position,
	/// returning if any collision happened, and the displacement
	/// in a specific angle.
	/// </summary>
	/// <param name="pos">Position.</param>
	public PhysicalInteraction collide(Vector3 pos, float objectWidth, bool isBelow)
	{
		//http://www.geometrictools.com/Documentation/DistancePoint3Triangle3.pdf
		PhysicalInteraction result = new PhysicalInteraction();
		result.collisionOccured = false;

		var width = tsf.localScale.x;
		var height = tsf.localScale.y;

		// scale to half size (since we move objectWidth both forward and backwards)
		objectWidth *= 0.5f;
		// set the center of the collision to the bottom.
		pos.y -= objectWidth;

		// get position on a unity scale.
		var x = (pos.x - (tsf.position.x - 0.5f * width)) / width;
		// the amount of normalized unity coordinates there is to a single line / point / waveform sample
		//var spaceBetweenPoints = width / numLines;
		// see if we're inside our bounds.
		if(x > 0 && x < 1)
		{
			var buf = buffer.getData();
			// okay, get the immediate index in the audio buffer:
			// be careful to keep integers unsigned, as we will be doing unsigned overflow
			// later.
			uint index = (uint)Mathf.RoundToInt(x * (buffer.getData().Length - 1));
			index = (index + bufC) & ((uint)(numLines) - 1);
			// create a baseline y
			var baselineY = tsf.position.y;

			// add a scope to clean up a bit
			{
				int numSamples = (int)(numLines * objectWidth / width);
				float currentMax = buf[index];   // set to first member.

				var lowerBound = (uint)Blunt.MathExt.confineTo((int)index - numSamples, 0, numLines - 1);
				var upperBound = (uint)Blunt.MathExt.confineTo((int)index + numSamples, 0, numLines - 1);

				//var lowerBound = index - (uint)numSamples & (uint)numLines - 1;
				//var upperBound = index + (uint)numSamples & (uint)numLines - 1;

				/*var lowest = System.Math.Min(lowerBound, upperBound);
				var highest = System.Math.Max(lowerBound, upperBound);
				lowerBound = lowest;
				upperBound = highest;*/
				// the argument x in f(x) that describes the shape of the collider
				var argument = ((float)lowerBound - index) / numSamples;
				// how much to increase the argument to the collision each sample.
				var reciprocalSamples = 1.0f / numSamples;

#if BLUNT_DEBUG
				Vector3 oldDebugCoord = new Vector3(pos.x + argument * objectWidth, pos.y + objectWidth);
#endif

				// here we select the highest point in the waveform around the objects x-position, subtracted
				// by the objects collider shape to find the highest y-point, that eventually will collide
				// with the object.
				bool didAnyRuns = false;
				while(lowerBound++ < upperBound)
				{
					didAnyRuns = true;
					// this is a simple second-degree polynomial, mimicking a circle.. somewhat.
					// add a sqrt() to circleDescriptor to get a perfect circle for collision.
					float circleDescriptor = objectWidth * argument * argument;

#if BLUNT_DEBUG
					// display the used collider in Unity
					Vector3 newDebugCoordinate = new Vector3(pos.x + argument * objectWidth, circleDescriptor + pos.y);
					Debug.DrawLine(oldDebugCoord, newDebugCoordinate, Color.red);
					oldDebugCoord = newDebugCoordinate;
#endif

					// check if this point is any higher
					if((buf[lowerBound] - circleDescriptor) > currentMax)
					{
						index = lowerBound;
						// store the relative horizontal angel of the collision.
						result.horizontalAngle = argument;
						currentMax = buf[lowerBound] - circleDescriptor;
					}
					argument += reciprocalSamples;
				}

				if(!didAnyRuns)
					print("Massive error");
			}
			// retrieve the highest relevant y-value
			var indexY = buffer.getData()[index];
			// see if the object is under this entry (ie., 'colliding' with the waveform)
			if(pos.y < (baselineY + indexY * height))
			{
				result.collisionOccured = true;
				result.displacement = new Vector3(objectWidth * -result.horizontalAngle, (baselineY + indexY * height) - pos.y);


			}
		}
		return result;
	}

}
