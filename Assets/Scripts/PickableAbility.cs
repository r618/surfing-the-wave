﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class PickableAbility : MonoBehaviour
{
	private ParticleSystem ps;
	private SpriteRenderer sr;
	private bool isPicked = false;
	// Use this for initialization
	void Start ()
	{
		ps = GetComponent<ParticleSystem>();
		sr = GetComponent<SpriteRenderer>();
	}

	private IEnumerator explode()
	{
		ps.emissionRate *= 5;
		sr.sprite = null;
		yield return new WaitForSeconds(0.2f);
		ps.emissionRate = 0;
		Destroy(gameObject, 5);

	}
	
	public void consume()
	{
		StartCoroutine(explode());
		if(!isPicked)
		{
			GameState.instance().onPickup();
			isPicked = true;
		}

	}

	// Update is called once per frame
	void Update () {
	
	}
}
