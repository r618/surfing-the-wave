﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MapGeneration : MonoBehaviour
{

	public GameObject pickupPrefab;
	public Vector3 playerStartPosition;
	public Vector3[] pickupPositions;
	public Player mainPlayer;

	private List<GameObject> spawnedObjectList;

	public GameObject[] gameObjectsToPlace;
	public Vector3[] gameObjectPositions;

	MapGeneration()
	{
		spawnedObjectList = new List<GameObject>();
	}

	// Use this for initialization
	void Start ()
	{
		GameState.instance().onGameStart += initializeMap;
		GameState.instance().onExit += destroyMap;
	}
	
	void initializeMap()
	{
		destroyMap();

		var rgd = mainPlayer.GetComponent<Rigidbody2D>();
		if(rgd)
		{
			rgd.position = playerStartPosition;
			rgd.velocity = new Vector3(0, 0, 0);
		}
		else
		{
			mainPlayer.transform.position = playerStartPosition;
		}

		foreach(Vector3 pos in pickupPositions)
		{
			spawnedObjectList.Add(GameObject.Instantiate(pickupPrefab, pos, Quaternion.identity) as GameObject);
		}

		if(gameObjectPositions.Length != gameObjectsToPlace.Length)
		{
			print("Error! -> mismatched amount of gameobjects and positions!");
		}

		for(int i = 0; i < gameObjectPositions.Length; ++i)
		{
			gameObjectsToPlace[i].GetComponent<Transform>().localPosition = gameObjectPositions[i];
			/*if(gameObjectsToPlace[i].rigidbody2D != null)
			{
				bool toggle = gameObjectsToPlace[i].rigidbody2D.isKinematic;
				gameObjectsToPlace[i].rigidbody2D.isKinematic = false;
                gameObjectsToPlace[i].rigidbody2D.position = gameObjectPositions[i];
				gameObjectsToPlace[i].rigidbody2D.isKinematic = toggle;
            }*/
		}

	}

	void destroyMap()
	{
		foreach(var obj in spawnedObjectList)
		{
			Destroy(obj);
		}
		spawnedObjectList.Clear();
	}

	// Update is called once per frame
	void Update ()
	{
	
	}
}
