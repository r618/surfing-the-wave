﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class WaterEffect : MonoBehaviour
{

	public FilterEffect f;
	public PhaserEffect p;

	public float depth = 0.95f;
	public float transitionTime = 0.9f;
	public bool effectsAreEnabled;
	public float cutoffLevel = 0.001f;

	public float currentDepth;
	private float depthVelocity;
	private float targetDepth;

	public void onWater()
	{
		targetDepth = depth;
	}

	public void onNormal()
	{
		targetDepth = 0.0f;
	}

	public void onGameLoaded()
	{
		currentDepth = depthVelocity = targetDepth = 0f;
		effectsAreEnabled = false;
	}

	// Use this for initialization
	void Start ()
	{
		GameState.instance().onGameIsLoaded += onGameLoaded;
		GameState.instance().onGameStart += onGameLoaded;
		GameState.instance().onWaterEntry += onWater;
		GameState.instance().onWaterExit += onNormal;
		GameState.instance().onUpdate += soundUpdate;
	}
	
	public void enableEffects(bool trigger)
	{
		effectsAreEnabled = f.isEnabled = p.isEnabled = trigger;
	}

	// Update is called once per frame
	void soundUpdate ()
	{
		currentDepth = Mathf.SmoothDamp(currentDepth, targetDepth, ref depthVelocity, transitionTime);
		if(currentDepth > cutoffLevel && !effectsAreEnabled)
		{
			enableEffects(true);
		}
		else if(currentDepth < cutoffLevel && effectsAreEnabled)
		{
			enableEffects(false);
		}

		p.depth = f.depth = currentDepth;
	}
}
