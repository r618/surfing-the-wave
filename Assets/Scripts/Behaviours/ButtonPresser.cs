﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class ButtonPresser : MonoBehaviour
{

	public event System.Action onButtonPress;
	public event System.Action onButtonRelease;

	public float retraction = 0.5f;
	public float retractionTime = 1f;
	float start, end;
	protected bool isRetracted = false;
	private bool isAnimating = false;
	public virtual void onButtonPressed()
	{
	}

	public virtual void onButtonFinished()
	{
		print("pressing button");
		if(onButtonPress != null)
			onButtonPress();
	}

	public void reset()
	{
		if(isRetracted)
		{
			StartCoroutine(AnimatePosition());
		}
	}

	IEnumerator AnimatePosition()
	{
		isAnimating = true;
		onButtonPressed();
		start = transform.localPosition.x;
		end = start + (isRetracted ? retraction : -retraction);
		float movement = end - start, timeStart = Time.time;
		while(true)
		{
			if(Time.time > timeStart + retractionTime)
				break;
			transform.localPosition = new Vector3(transform.localPosition.x + movement * Time.deltaTime / retractionTime, transform.localPosition.y);
			yield return null;
		}

		isRetracted = false;
		isAnimating = false;
		onButtonFinished();
	}

	public void OnCollisionEnter2D(Collision2D c)
	{
		if(!isAnimating)
			StartCoroutine(AnimatePosition());
	}

	public void OnCollisionStay2D(Collision2D c)
	{
	}

	public void OnCollisionExit2D(Collision2D c)
	{

	}
}
