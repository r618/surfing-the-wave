﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;

public class Diagnostics : MonoBehaviour {

	// Use this for initialization
	float deltaTime = 0.0f;
	public Blunt.Sound.Mixer mainMixer;
	void Update()
	{
		if(Time.timeScale == GameState.instance().currentTimeScale)
			deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
	}
	
	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;
		
		GUIStyle style = new GUIStyle();
		
		Rect rect = new Rect(0, 0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 2 / 100;
		style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		float accumulatedCPU = 0.0f;
		if(mainMixer != null)
		{

			// set the mixer to enable measurements.
			mainMixer.enableMixerMeasurements(true);

			var readings = mainMixer.getReadings();



			foreach(Blunt.Sound.Mixer.Reading channel in readings)
			{

				var timeForProcessing = channel.processingTime;

				var cpuTime = timeToCPUUsage(timeForProcessing, channel.sampleFrames,
												Blunt.Synthesis.EnvironmentTuning.sampleRate);

				accumulatedCPU += (float)cpuTime;
			}
		

		}
		string text = string.Format("{0:0.0} ms ({1:0.} fps) - {2:0.00}% core (audio)", msec, fps, accumulatedCPU);
		GUI.Label(rect, text, style);
	}

	double timeToCPUUsage(double processingTime, double sampleFrames, double sampleRate)
	{
		var timePerSample = processingTime / sampleFrames;
		var msForASecond = timePerSample * sampleRate;
		return 0.1 * msForASecond;   // divided by 1000, times 100 for percentage.
	}
}
