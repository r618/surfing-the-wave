﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

#define BLUNT_DEBUG

using UnityEngine;
using System.Collections;

using DSPType = System.Single;


public class Waveform : MonoBehaviour, Blunt.Sound.SoundStage
{
	public Color c1 = Color.yellow;
	public Color c2 = Color.red;
	public float historyLength;
	public int numLines = 128;
	public Blunt.Sound.Mixer mixer;
	public Material m;
	public string channelToListenTo = "main";
	public bool isBehindStuff = false;
	public Transform parentTransform;
	public float lineWidth = 0.01f;
	public float amplification = 1.0f;
	public float distanceToUpdate = 15f;
	protected DSPType currentSample = 0.0f;
	protected uint bufC = 0;
	protected uint dsCounter = 0;
	protected LineRenderer rasterizer;
	protected Blunt.CResizableContainer<DSPType> buffer
		= new Blunt.CResizableContainer<DSPType>();
	protected Transform tsf;
	protected double timeAtLastProcessing;
	protected System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
	public float distanceToPlayer;

	protected bool isRasterizerEnabled;

	void Start()
	{
		isRasterizerEnabled = false;
		rasterizer = gameObject.AddComponent<LineRenderer>();
		if(m == null)
			rasterizer.material = new Material(Shader.Find("Particles/Additive"));
		else
			rasterizer.material = m;
		rasterizer.SetColors(c1, c2);
		rasterizer.SetWidth(lineWidth, lineWidth);
		rasterizer.SetVertexCount(numLines);
		rasterizer.useWorldSpace = false;
		buffer.ensureSize(numLines);
		if(isBehindStuff)
			rasterizer.sortingOrder = -1;

		tsf = GetComponent<Transform>();

		if(parentTransform != null)
			tsf.parent = parentTransform;

		GameState.instance().onGameIsLoaded += onAudioIsLoaded;

	}

	private void onAudioIsLoaded()
	{
		mixer.addListenerForChannel(channelToListenTo, this);
	}

	public void process(float[] data, int nSampleFrames, int channels, bool channelIsEmpty)
	{
		if(channels == 2)
		{
			watch.Reset();
			watch.Start();
			uint samplesPerLine =
				(uint)Mathf.RoundToInt(Blunt.Synthesis.EnvironmentTuning.sampleRate * historyLength / numLines);

			var buf = buffer.getData();
			for(int i = 0; i < nSampleFrames * channels; i += channels)
			{
				dsCounter++;

				// see Blunt.MathExt.AbsMax
				//if(data[i] * data[i] > currentSample * currentSample)
				currentSample = data[i];

				if(dsCounter > samplesPerLine)
				{
					dsCounter -= samplesPerLine;
					buf[bufC] = currentSample;
					bufC++;
					bufC &= (uint)(numLines) - 1;
					currentSample = 0.0f;

				}

			}
		}
	}


	void Update()
	{
		distanceToPlayer = (GameState.instance().mainPlayer.transform.position - transform.position).magnitude;
        if(distanceToPlayer < distanceToUpdate)
		{
			if(!isRasterizerEnabled)
			{
				rasterizer.enabled = true;
				isRasterizerEnabled = true;
			}
			double numElapsedIndexes = watch.Elapsed.TotalSeconds * Blunt.Synthesis.EnvironmentTuning.sampleRate;

			var recLength = 1.0f / numLines;

			uint offset = (uint)Mathf.RoundToInt((float)(numElapsedIndexes * recLength)) + bufC;
			int bufOffset = (int)(numElapsedIndexes * recLength) + 2;
			//print ("Offset = " + offset + ", Elapsed samples: " + numElapsedIndexes + ", indices: " + numElapsedIndexes * recLength);
			var buf = buffer.getData();

			var z = isBehindStuff ? 10f : 1f;
			int i = 0;
			for(; i < numLines - bufOffset; ++i)
			{
				var bufIndex = ((uint)(i) + offset) & ((uint)(numLines) - 1);
				Vector3 pos = new Vector3(i * recLength - 0.5f, buf[bufIndex] * amplification, z);

				rasterizer.SetPosition(i, pos);

			}

			for(; i < numLines; ++i)
			{
				Vector3 pos = new Vector3(i * recLength - 0.5f, 0, z);

				rasterizer.SetPosition(i, pos);
			}
		}
		else
		{
			if(isRasterizerEnabled)
			{
				isRasterizerEnabled = false;
				rasterizer.enabled = false;
			}
		}
	}
}
