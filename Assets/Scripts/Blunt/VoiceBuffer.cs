﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;

namespace Blunt.Synthesis
{
	public class VoiceBuffer<VoiceType> where VoiceType : Synthesis.Voice
	{
		private int size;
		protected CFrontQueue<VoiceType> voices = new CFrontQueue<VoiceType>();
		protected Synthesizer hostSynth;

		public VoiceBuffer(Synthesis.Synthesizer synthesizerToHost)
		{
			hostSynth = synthesizerToHost;
		}

		public void initialize(int bufferSize, System.Action<VoiceType> initializer)
		{
			// kill and remove all voices.
			foreachVoice(
				voice =>
				{
					voice.stop();
					hostSynth.removeVoice(voice);
				}
            );
			// delete voices.
			voices.clear();
			// size buffer.
			for(int i = 0; i < bufferSize; ++i)
			{
                voices.push((VoiceType)hostSynth.createVoice(EnvironmentTuning.A4));
			}
			foreachVoice(initializer);
		}
		public virtual VoiceType stealOldestVoice()
		{
			var v = voices.pop();
			voices.push(v);
			return v;
		}

		public virtual VoiceType getCurrent()
		{
			return voices.newest();
		}

		public void foreachVoice(System.Action<VoiceType> operation)
		{
			foreach(var v in voices)
			{
				operation(v);
			}
		}
	}
}