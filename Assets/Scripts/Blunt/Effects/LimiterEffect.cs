/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

#define BLUNT_DEBUG

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Blunt
{
	namespace Sound
	{
		using DSPType = System.Single;
		using SystemFloat = System.Single;

		public class LimiterEffect : MonoBehaviour
		{
			// don't we just love single inheritance, 
			// forcing us to reimplement nearly everything 
			// as composition.. ?
			public DSPType limitingThreshold = 0.95f;	// 0 .. 1
			public DSPType release = 50;   // 0 .. 1000
			private Limiter effect = new Limiter();
			// display:
			public DSPType currentGain;
			public DSPType currentReductionInDBs;
			public void OnAudioFilterRead(SystemFloat[] data, int nChannels)
			{

				effect.release = release;
				effect.limitingThreshold = limitingThreshold;
				effect.process(data, data.Length / nChannels, nChannels, false);
				currentGain = effect.currentGain;
				currentReductionInDBs = MathExt.fractionToDB(currentGain);
            }
		}
		/// <summary>
		/// Limiting is basically a special case of 		
		/// compression, where your ratio is infinite and   
		/// your attack is zero.
		/// </summary>
		public class Limiter : SoundStage
		{
			public DSPType limitingThreshold = 0.95f; // 0 .. 1
			public DSPType envelope = 0.0f;   // envelope follower
			public DSPType release = 50;   // 0 .. 1000

			// utility
			public DSPType currentGain;
			public void process(SystemFloat[] data, int nSampleFrames, int channels, bool channelIsEmpty)
			{
				// this is our temporary sample
				float sample;
				// the gain we apply
				float gain = 1.0f;
				// our envelope 
				float env = envelope;
				// our threshold 
				float threshold = limitingThreshold * limitingThreshold;
				// how much the envelope decays each sample after the input volume has fallen below threshold
				float decay = Mathf.Exp(-1.0f / (Synthesis.EnvironmentTuning.sampleRate * release));
				int bufsize = nSampleFrames * channels;
				float s1, s2;
				// main loop
				for(int i = 0; i < bufsize; i += channels)
				{
					// catch the sample we evaluate.
					s1 = data[i];
					s2 = data[i + 1];
					sample = Mathf.Abs(MathExt.AbsMax(data[i], data[i + 1]));
					// reset gain
					gain = 1.0f;
					// check if input volume is above our threshold
					if(sample > threshold)
					{
						// this is analogous to zero attack (because the envelope is instantly set to the input volume)
						if(sample > env)
							env = sample;
					}
					// apply gain only if volume envelope is above zero, else gain spins out of control
					if(env > threshold)
						gain = threshold/env;
					// apply release to envelope if input is falling below threshold
					if(sample < threshold)
						env *= decay;
					// lastly, gain outputs!
					data[i] *= gain;
					data[i + 1] *= gain;

				}
				// save the envelope variable
				envelope = env;
				currentGain = gain;
			}


		};

	}
}