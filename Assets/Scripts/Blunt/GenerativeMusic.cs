﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Blunt.Synthesis;

namespace Blunt.Sound.GenerativeMusic 
{

	public class CallbackSequencer : FMSynth.SequenceCallback
	{
		public class Token
		{
			System.Action<Token, PlayHead> callback;
			PlayHead ph;

			public Token(System.Action<Token, PlayHead> callbackFunction, PlayHead p)
			{
				callback = callbackFunction;
				ph = p;
			}
			public void callbackIn(PlayHead.TimeOffset t)
			{
				isCallbackPending = true;
				timeStamp = ph.getCurrentOffset();
				deltaTime = t;
			}

			public void disableAnyCallbacks()
			{
				isCallbackPending = false;
			}

			public void resumeCallbacks(PlayHead p, PlayHead.TimeOffset t)
			{
				if(deltaTime.beats != 0.0)
				{
					timeStamp = p.getCurrentOffset();
					deltaTime += t;
					isCallbackPending = true;
				}
			}

			public void doCallback(PlayHead newPlayhead)
			{
				ph = newPlayhead;
				isCallbackPending = false;
				timeStamp = deltaTime = 0;
				callback(this, ph);
			}

			public PlayHead.TimeOffset timeStamp;
			public PlayHead.TimeOffset deltaTime;
			public bool isCallbackPending;
		}

		private List<Token> tokens;
		private FMSynth synth;

		public CallbackSequencer(FMSynth s)
		{
			synth = s;
			synth.addSequencer(this);
			tokens = new List<Token>();
		}

		public Token createCallbackToken(System.Action<Token, PlayHead> callbackFunction)
		{
			Token ret = new Token(callbackFunction, synth.getPlayHead());
			tokens.Add(ret);
			return ret;
		}

		public void removeToken(Token t)
		{
			tokens.Remove(t);
		}

		public void onSequencing(FMSynth s, PlayHead p)
		{
			var currentTiming = p.preciseTiming;

			for(int i = 0; i < tokens.Count; ++i)
			{
				if(tokens[i].isCallbackPending && currentTiming >= (tokens[i].timeStamp + tokens[i].deltaTime).beats)
				{
					tokens[i].doCallback(p);
				}
			}
		}
	}


	public class MusicPiece
	{
		public class Instrument
		{
			public System.Func<PlayHead, Sequencer.NoteInfo[]> harmonicGenerator;
			public Blunt.Probability.Music.RandomSequenceList rsl;
			public Sequencer sequencer;
			public VoiceBuffer<FMSynth.Voice> buffer;
			public CallbackSequencer.Token token;

			public void onTokenCallback(CallbackSequencer.Token t, PlayHead p)
			{

				if(rsl.isStartAndNotZero(p))
				{
					t.callbackIn(rsl.getNextMoment(p));
				}
				else
				{
					double nextMoment = rsl.getNextMoment(p);
					// get (if any) notes for this sequence point in time
					var notes = harmonicGenerator(p);
					// play them
					sequencer.playNotes(buffer, notes);
					// ciao
					t.callbackIn(nextMoment);
				}

			}
			
		}

		private List<Instrument> instruments = new List<Instrument>();
		private Sequencer sequencer;
		private CallbackSequencer cbseq;
		private CallbackSequencer.Token stopToken;
		private FMSynth synth;
		bool isPlaying;

		public Instrument createAndAddInstrument(VoiceBuffer<FMSynth.Voice> voiceBuf, Blunt.Probability.Music.RandomSequenceList rhythm, System.Func<PlayHead, Sequencer.NoteInfo[]> harmonicMusicGenerator)
		{
			Instrument ret = new Instrument();
			ret.buffer = voiceBuf;
			ret.rsl = rhythm;
			ret.harmonicGenerator = harmonicMusicGenerator;
			ret.sequencer = sequencer;
			ret.token = cbseq.createCallbackToken(ret.onTokenCallback);
			instruments.Add(ret);
			return ret;
		}


		public bool isCurrentlyPlaying()
		{
			return isPlaying;
		}
		public void playPiece(PlayHead.TimeOffset delay)
		{
			if(!isPlaying)
			{
				foreach(Instrument i in instruments)
				{
					i.token.callbackIn(delay);
				}
				isPlaying = true;
			}
		}

		public void resumePiece(PlayHead.TimeOffset t)
		{
			if(!isPlaying)
			{
				foreach(Instrument i in instruments)
				{
					i.token.resumeCallbacks(synth.getPlayHead(), t);
				}
				isPlaying = true;
			}
		}

		private void delayedStopCall(CallbackSequencer.Token t, PlayHead p)
		{
			foreach(Instrument i in instruments)
			{
				i.token.disableAnyCallbacks();
				i.rsl.resetSequencer();
			}
			isPlaying = false;
			cbseq.removeToken(stopToken);
		}

		public void stopPiece(PlayHead.TimeOffset delay)
		{
			if(isPlaying)
			{
				isPlaying = false;
				stopToken = cbseq.createCallbackToken(delayedStopCall);
				stopToken.callbackIn(delay);
			}
		}

		public MusicPiece(Sequencer s, CallbackSequencer cbs, FMSynth hostSynth)
		{
			isPlaying = false;
			sequencer = s;
			cbseq = cbs;
			synth = hostSynth;
		}

	}

}
