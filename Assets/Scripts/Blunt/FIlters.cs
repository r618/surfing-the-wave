﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Blunt
{
	using DSPType = System.Single;
	namespace Filters
	{
		// standard complex rotation
		public struct ComplexResonator
		{
			public DSPType cosine;
			public DSPType sine;
			public DSPType x;
			public DSPType y;
		};

		// simple Goertzel-like IIR filter
		public struct SimpleOscillator
		{
			public DSPType coeff;
			public DSPType d1, d2;
			public DSPType omega;


		};
		public class Design
		{

			public static ComplexResonator designResonator(DSPType rotationSpeed, DSPType initialPhase)
			{
				ComplexResonator ret;
				ret.cosine = (DSPType)Math.Cos (rotationSpeed);
				ret.sine = (DSPType)Math.Sin (rotationSpeed);
				ret.x = (DSPType)Math.Cos(initialPhase);
				ret.y = (DSPType)Math.Sin (initialPhase);
				return ret;

			}
			public static ComplexResonator designResonator(DSPType frequency)
			{
				return designResonator ((DSPType)(2 * Math.PI * frequency / Synthesis.EnvironmentTuning.sampleRate), 0.0f);
			}

			public static SimpleOscillator designOscillator(DSPType frequency)
			{
				SimpleOscillator ret;
				double omega = 2 * Math.PI * frequency / Synthesis.EnvironmentTuning.sampleRate;
				ret.coeff = 2 * (DSPType)Math.Cos (omega);
				ret.d1 = (DSPType)Math.Cos (-omega - Math.PI * 0.5);
				ret.d2 = (DSPType)Math.Cos (-2 * omega - Math.PI * 0.5);
				ret.omega = (DSPType)omega;
				return ret;
			}

		};


	};

}