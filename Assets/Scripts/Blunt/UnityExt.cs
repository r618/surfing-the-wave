﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System;

namespace Blunt.UnityExt 
{
	public static class UnityExtensions
	{
		static private IEnumerator callback(Action a, float delayInSeconds)
		{
			if(delayInSeconds > 0)
			{
				yield return new WaitForSeconds(delayInSeconds);
			}

			a();
		}

		static public void delayedAction(this MonoBehaviour m, Action a, float secondsToDelay)
		{
			m.StartCoroutine(callback(a, secondsToDelay));
		}

		static public Vector3 withZ(this Vector3 v, float Z = 0)
		{
			return new Vector3(v.x, v.y, Z);
		}

		
	}
}
