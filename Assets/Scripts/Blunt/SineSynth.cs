﻿/*************************************************************************************

	blunt - a dull C# library - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	See \licenses\ for additional details on licenses associated with this program.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace Blunt
{
	using DSPType = System.Single;
	using SystemFloat = System.Single;

	namespace Synthesis
	{

		public class SineSynth : Synthesizer, Sound.SoundStage
		{
			public class Voice : Synthesis.Voice
			{

				public Filters.SimpleOscillator res;
				public override void setPitch(DSPType frequency)
				{
					res = Filters.Design.designOscillator(frequency);
				}

				public override void setPitch(int midiNote, DSPType cents)
				{
					res = Filters.Design.designOscillator(EnvironmentTuning.pitchFromNote(midiNote, cents));
				}
				public override void play(float volumeInDBs)
				{

					/*	TO DO: if we are already playing this voice, should we start over at the same point in volume (thus, making the attack smaller)
					 *  or do as now, where we clip the note?
					 * if (enabled) 
					{
						switch(adsr.stage)
						{
							case ADSREnvelope.Stage.


						};
					}
					else*/
					{
						enabled = true;
						this.volume = MathExt.dbToFraction(volumeInDBs);
						adsr.stage = ADSREnvelope.Stage.Attack;
						adsr.deltaSamples = 0;
						instrument.addVoice(this);
						timeAtPlay = Time.time;
					}
				}

				public override void stop()
				{
					base.stop();
					instrument.removeVoice(this);
				}

				public Voice(SineSynth s)
					: base(s)
				{
					adsr.deltaSamples = adsr.attackTime = 0;
					adsr.decayPole = adsr.releasePole = adsr.auxVol = 0;
					adsr.sustainLevel = 1;
					adsr.auxVol = 0;
					adsr.stage = ADSREnvelope.Stage.Attack;
				}

			}

			private List<Voice> voices = new List<Voice>();
			private DSPType cutoffThreshold;

			// only for display!
			public int numVoices = 0;
			public int numActiveVoices = 0;
			
			public enum OscillatorType
			{
				Pure,
				FM,
				AM, 
			};
			
			public enum WaveformShape
			{
				Sine,
				Noise
			};
			
			
			public void addVoice(Synthesis.Voice newVoice)
			{
				Voice v = newVoice as Voice;
				if(v != null)
				{
					lock(voices)
					{
						voices.Add(v);
					}
				}
			}
			
			public void removeVoice(Synthesis.Voice newVoice)
			{
				Voice v = newVoice as Voice;
				if(v != null)
				{
					lock(voices)
					{
						voices.Remove(v);
					}
				}
			}
			
			public SineSynth () 
			{
				EnvironmentTuning.updateFields ();
				setCutThreshold (-70);
			}
			
			void Update()
			{
				// remove all disabled voices:
				lock(voices)
				{
					voices.RemoveAll (i => !i.enabled);
				}
				
			}
			
			public void setCutThreshold(DSPType cutoffInDBs)
			{
				cutoffThreshold = (DSPType)Math.Abs (MathExt.dbToFraction(cutoffInDBs));
			}
			
			public Synthesis.Voice createVoice(int midiNote, DSPType detuneInCents = 0)
			{
				Voice v = new Voice (this);
				v.midiNote = midiNote;
				v.res = Filters.Design.designOscillator (EnvironmentTuning.pitchFromNote (midiNote, detuneInCents));
				//voices.Add (v);
				numVoices++;
				return v;
			}
			
			public Synthesis.Voice createVoice()
			{
				Voice v = new Voice (this);
				
				numVoices++;
				return v;
			}

			public void process (float[] data, int nSampleFrames, int channels, bool channelIsEmpty)
			{
				lock(voices)
				{

					if (channels == 2)
					{
						voices.RemoveAll (i => !i.enabled);
						foreach(Voice v in voices)
						{
							if(!v.enabled)
								continue;
							DSPType coeff = v.res.coeff;
							DSPType d1 = v.res.d1;
							DSPType d2 = v.res.d2;
							DSPType cosine = 0;
							//DSPType volume = v.volume;
							var leftMaster = (-v.pan + 1) * 0.5f * v.volume * v.masterVolume;
							var rightMaster = (v.pan + 1) * 0.5f * v.volume * v.masterVolume;
							DSPType currentVolume = 0;
							int j = 0;
							int len = nSampleFrames * channels;
							do
							{
				
								int deltaSamples = v.adsr.deltaSamples;
								switch(v.adsr.stage)
								{
									case ADSREnvelope.Stage.Attack:
									{
										// we do linear ramping for attacks.
										DSPType attack = v.adsr.attackTime;
										// special case; this also needs to handle following NaNs
										if(attack == 0)
										{

											deltaSamples = 0;
											v.adsr.stage = ADSREnvelope.Stage.Decay;
											// we reached maximum volume immediately
											currentVolume = 1;
											
										}
										else
										{
											DSPType reciprocalAttack = (DSPType)(1.0) / attack;
											currentVolume = v.adsr.deltaSamples * reciprocalAttack;
											for(; j < len; j += channels)
											{
												currentVolume += reciprocalAttack;
												cosine = coeff * d1 - d2;
												d2 = d1;
												d1 = cosine;
												
												// do left:
												data[j] += cosine * leftMaster * currentVolume;
												// and right:
												data[j + 1] += cosine * rightMaster * currentVolume;


												deltaSamples++;
												if(deltaSamples >= attack)
												{
													deltaSamples = 0;
													v.adsr.stage = ADSREnvelope.Stage.Decay;
													// we did actuall process this sample.
													j += channels;
													currentVolume -= v.adsr.sustainLevel;

													break;
												}
											}
										}
										// save progress
										v.adsr.auxVol = currentVolume;
										v.adsr.deltaSamples = deltaSamples;
										break;
									}
									case ADSREnvelope.Stage.Decay:
									{
										// And decays.
										currentVolume = v.adsr.auxVol;
										DSPType pole = v.adsr.decayPole;
										DSPType sustain = v.adsr.sustainLevel;
										for(; j < len; j += channels)
										{
											cosine = coeff * d1 - d2;
											d2 = d1;
											d1 = cosine;
											
											currentVolume *= pole;

											// do left:
											data[j] += cosine * leftMaster * (currentVolume + sustain);
											// and right:
											data[j + 1] += cosine * rightMaster * (currentVolume + sustain);
											
										}
										// save progress
										v.adsr.auxVol = currentVolume;
										// set currentVolume to something sensible, to avoid being cut:
										currentVolume += sustain;
										break;
									}
									case ADSREnvelope.Stage.Release:
									{
										// And decays.
										currentVolume = v.adsr.auxVol;
										DSPType pole = v.adsr.releasePole;
										for(; j < len; j += channels)
										{
											cosine = coeff * d1 - d2;
											d2 = d1;
											d1 = cosine;
											
											currentVolume *= pole;
											
											// do left:
											data[j] += cosine * leftMaster * (currentVolume);
											// and right:
											data[j + 1] += cosine * rightMaster * (currentVolume );
											
										}
										// save progress
										v.adsr.auxVol = currentVolume;

										break;
									}
									default:
									{
										// this should REALLY not happen, but we don't wanna hang the audio thread!
										// we end up here if memory somehow got corrupted.
										j = data.Length;
										break;
									}	
								};

							} while(j < data.Length);

							v.res.d1 = d1;
							v.res.d2 = d2;

							// stop silent voices, so they'll be removed on update.
							if((0.5 * Math.Abs (currentVolume * leftMaster + currentVolume * rightMaster)) < (cutoffThreshold))
							{
							   v.enabled = false;
							}
						}


					}
				}
				this.numActiveVoices = voices.Count;

					/*
					DSPType x = v.res.x;
					DSPType y = v.res.y;
					DSPType z = x;

					DSPType sine = v.res.sine;
					DSPType cosine = v.res.cosine;

					for(int i = 0; i < data.Length; ++i)
					{
						// complex filter resonation rotation.
						z = x * cosine - y * sine;
						y = x * sine + y * cosine;
						x = z;
						data[i] = (SystemFloat)z;
						
					}

					v.res.x = x;
					v.res.y = y;
					*/
					/*
					DSPType pitch = EnvironmentTuning.pitchFromNote(v.midiNote);
					DSPType rotation = 2 * Mathf.PI * pitch / EnvironmentTuning.sampleRate;
					DSPType phase = v.phase;
					for(int i = 0; i < data.Length; ++i)
					{
						DSPType sample = Mathf.Sin(phase);
						phase += rotation;
						data[i] = sample;
						
					}
					v.phase = phase;
					*/

			}



		}
	}
}