﻿/*************************************************************************************

	Surfing The Wave - Interactive Procedural Sound Design Game - v. 0.1.0.

	Copyright (C) 2015 Janus Lynggaard Thorborg [LightBridge Studios]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************/

using UnityEngine;
using System.Collections;
using Blunt.Synthesis;
/// <summary>
/// This class is responsible for controlling the game, delegating certain events, ensuring states, etc.
/// </summary>
public class GameController : MonoBehaviour
{


	
	void initializeAudioConfigurations()
	{
		// initialize audio subsystem
		EnvironmentTuning.updateFields();
		// initialize, pre-compile and hotspot common jit-locations.
		FMSynth synth = new FMSynth();
		Blunt.Sound.Limiter l = new Blunt.Sound.Limiter();

		for(int i = 0; i < 20; ++i)
		{
			var voice = (FMSynth.Voice)synth.createVoice(64);
			// utilize most codepaths here..
			voice.createAndAddOperator();
			voice.createAndAddFilter(FMSynth.Voice.Operator.FilterTypes.LP, 20, 5);
			voice.createAndAddParameter(5);
			voice.createAndAddRandomOffset().isModulator = true;
			voice.createAndAddOperator().kind = FMSynth.Voice.Operator.Kind.Noise;
			var buf = new Blunt.CResizableContainer<float>();
			buf.ensureSize(4096);
			synth.process(buf.getData(), 2048, 2, false);
			l.process(buf.getData(), 2048, 2, false);
		}

	}

	// Update is called once per frame
	void Update ()
	{
	
	}
}
